![license](https://img.shields.io/badge/license-%20GNU%20GPLv3-yellow.svg)

# README #
This README describes the project and its (bioinformatics) analysis.
Version 1.0

## Team members ##
Yvonne Muller <br />
Thijs J. Schrama  <br />
Giovanna Jona Lasinio, Statistician <br />
Dwin Grashof, Bioinformatician Computational Biology & Bioinformatics Immunology<br />
Harmen van de Werken, Head Computational Biology & Bioinformatics Immunology<br />
Peter Katsikis, PI

## Stratification of hospitalized COVID-19 patients into clinical severity progression groups by immuno-phenotyping and machine learning ##
The publication can be found on Nature Communications: DOI: <a href ="https://doi.org/10.1038/s41467-022-28621-0">https://doi.org/10.1038/s41467-022-28621-0</a></br >
Please cite:</br >
Mueller, Y. M., Schrama, T. J., Ruijten, R., Schreurs, M. W. J., Grashof, D. G., van de Werken, H. J. G., Jona Lasino G.,Alvarez-Sierra, D., Kiernan, C. H., Castro Eiro, M. D., van Meurs, M., Brouwers-Haspels, I., Zhao, M., Li, L., de Wit, H., Ouzounis, C. A., Wilmsen, M. E. P., Alofs, T., Laport, D. A., van Wees, T., Kraker, G., Jaimes, M. C., Van Bockstael, S., Hernandez-Gonzalez, M., Rokx, C., Rijnders, B. J. A., Pujol-Borrell, R. & Katsikis, P. D. </br >
Immunophenotyping and machine learning identify distinct immunotypes that predict COVID-19 clinical severity. </br >
Nat Commun 13,915 (2022). doi:10.1038/s41467-022-28621-0</br >


### Abstract ###
Quantitative or qualitative differences in immunity may drive clinical severity in COVID-19. 
Although longitudinal studies to record the course of immunological changes are ample, they do not necessarily predict clinical progression at the time of hospital admission. 
Here we show, by a machine learning approach using serum pro-inflammatory, anti-inflammatory and anti-viral cytokine and anti-SARS-CoV-2 antibody measurements as input data, that COVID-19 patients cluster into three distinct immune phenotype groups. 
These immune-types, determined by unsupervised hierarchical clustering that is agnostic to severity, predict clinical course. 
The identified immune-types do not associate with disease duration at hospital admittance, but rather reflect variations in the nature and kinetics of individual patient’s immune response. 
Thus, our work provides an immune-type based scheme to stratify COVID-19 patients at hospital admittance into high and low risk clinical categories with distinct cytokine and antibody profiles that may guide personalized therapy.

### Content ###
This project contains custom R scripts / workflows used to perform machine learning based immunophenotyping on SARS-CoV-2 positive patients of Rotterdam (*N* = 50) as a first cohort and Barcelona (*N* = 88) as validation cohort.
The data used in this study are available on request from the corresponding author Peter Katsikis. The data are not publicly available due to participant privacy/consent.

This workflow (in R) was performed on Ubuntu 20.04.2 LTS with the following R version:
```R
R version 4.1.2 (2021-05-18)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Ubuntu 20.04.2 LTS
```

### Required R packages
* [pacman](https://cran.r-project.org/web/packages/pacman/index.html)
	* Additional R packages are installed via _pacman_.

### Running Workflow
After requesting and acquiring the data, or formatting your own data as an excel file in a way that the patients are presented in the rows and the to be analysed variables in the columns, 
the excel file can be loaded into R in the first section _Make data_. Assuming the publication data is used one should adjust the path `ExtData/Input/DATA_used_Immunotypes_03082021.xlsx` to the corresponding path of the excel file.
Additionaly an empty outputfolder should be present with the relative path `ExtData/Output/` to the R markdown file. 
Afterwards the scripts can be performed in the order listed in the R/ folder to generate all the respective data and corresponding figures of the publication.
